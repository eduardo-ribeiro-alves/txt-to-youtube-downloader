from pytube import YouTube
from youtubesearchpython import VideosSearch

def get_file_item():
    lyrics = True
    music_list = open("my_spotify_lib.txt", encoding="utf8")
    
    if music_list.readline() == "off":
    	lyrics = False
    
    for music in music_list:
        if lyrics:
            new_name = music + " Lyrics"
        
        get_url(new_name)

def get_url(music_name):
    videosSearch = VideosSearch(music_name, limit = 2)
    video_url = "https://www.youtube.com/watch?v=" + videosSearch.result()['result'][0]['id']

    download_music(video_url)

def download_music(video_url):
    yt = YouTube(video_url)
    audio = yt.streams.filter(only_audio=True).first()
    audio.download(r'C:\Musica')

get_file_item()

